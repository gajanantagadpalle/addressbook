<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->binInteger('uid', 10);
            $table->string('tofromid', 7);
            $table->integer('titleid');
            $table->string('person_name');
            $table->string('person_phone');
            $table->text('address_line_1');
            $table->text('address_line_2');
            $table->text('address_line_3');
            $table->string('pincode', 10);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->string('country', 3);
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->comment('0=>Inactive, 1=>Active, 5=>Deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
