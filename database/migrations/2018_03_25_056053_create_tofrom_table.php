<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTofromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tofrom', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('code', 7);
            $table->tinyInteger('status')->default(1)->comment('0=>Inactive, 1=>Active, 5=>Deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tofrom');
    }
}
