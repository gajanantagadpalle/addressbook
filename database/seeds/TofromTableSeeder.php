<?php

use Illuminate\Database\Seeder;

class TofromTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tofrom')->insert(
			array(
                array('code'=>'to_from', 'name'=>'To Address & From Address', 'status'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')),
                array('code'=>'to', 'name'=>'To Address', 'status'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')),
                array('code'=>'from', 'name'=>'From Address', 'status'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')),
            )
        );
    }
}
