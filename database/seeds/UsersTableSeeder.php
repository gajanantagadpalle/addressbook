<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert(
			array(
                array('id' => '1','name' => 'Gajanan Tagadpalle','email' => 'gajanantagadpalle@gmail.com','password' => '$2y$10$vY8I4wHwHFnwLa5JhfOn/eI98I3pac/.iAU.507tdjsBg2feWw5aC','remember_token' => NULL,'created_at' => '2018-03-25 13:06:03','updated_at' => '2018-03-25 13:06:03','username' => 'gajanantagadpalle','api_token' => '7vun4hLywi8Xehu1uHvls4ctWpudDcNdZQ1aoGGApblRkOuUOcsJSXilqblX')
            )
        );
    }
}
