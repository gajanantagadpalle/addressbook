<?php

use Illuminate\Database\Seeder;

class AddresstitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('addresstitles')->insert(
			array(
                array('title'=>'Home', 'status'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')),
                array('title'=>'Work', 'status'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')),

            )
        );
    }
}
