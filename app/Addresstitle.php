<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addresstitle extends Model{
    protected $fillable = [
        'title', 'status'
    ];
}
