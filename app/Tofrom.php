<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tofrom extends Model{
    protected $table = 'tofrom';
    protected $fillable = [
        'name', 'code', 'statue'
    ];
}
