<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::post('auth/login', 'Auth\AuthController@authenticate');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


Route::get('docs', ['as' => 'docs', 'uses' => 'CommonController@docs']);
Route::match(['get'], 'testgetaddresslist', ['as' => 'testgetaddresslist', 'uses' => 'CommonController@testGetAddressList']);
Route::match(['get'], 'testdeleteaddress', ['as' => 'testdeleteaddress', 'uses' => 'CommonController@testDeleteAddress']);
Route::match(['get'], 'testgetaddressdetails', ['as' => 'testgetaddressdetails', 'uses' => 'CommonController@testGetAddressDetails']);
Route::match(['get'], 'testaddaddress', ['as' => 'testaddaddress', 'uses' => 'CommonController@testAddAddress']);
Route::match(['get'], 'testupdateaddress', ['as' => 'testupdateaddress', 'uses' => 'CommonController@testUpdateAddress']);


Route::group(['middleware' => 'auth'], function(){
    Route::get('/', 'WelcomeController@index');
    Route::get('home', 'HomeController@index');
    Route::get('addresslist', ['as' => 'addresslist', 'uses' => 'AddressController@AddressList']);

    Route::get('addaddress/{id?}', ['as' => 'addaddress', 'uses' => 'AddressController@AddAddress']);
    Route::get('profile', ['as' => 'profile', 'uses' => 'UserController@Profile']);
    Route::match(['get', 'post'], 'profile', ['as' => 'profile', 'uses' => 'UserController@Profile']);
    Route::match(['get', 'post'], 'changepassword', ['as' => 'changepassword', 'uses' => 'UserController@ChangePassword']);
    Route::match(['post'], 'actionaddaddress', ['as' => 'actionaddaddress', 'uses' => 'AddressController@ActionAddAddress']);
    Route::match(['post'], 'actiondeladdress', ['as' => 'actiondeladdress', 'uses' => 'AddressController@ActionDelAddress']);




});

Route::group(['middleware' => 'api'], function(){
    Route::group(['prefix' => 'api'], function(){
        Route::get('getaddresslist', ['as' => 'getaddresslist', 'uses' => 'ApiController@getAddressList']);
        Route::get('getaddressdetails', ['as' => 'getaddressdetails', 'uses' => 'ApiController@getAddressDetails']);
        Route::delete('deleteaddress', ['as' => 'deleteaddress', 'uses' => 'ApiController@deleteAddress']);
        Route::post('addaddress', ['as' => 'apiaddaddress', 'uses' => 'ApiController@AddAddress']);
    });
});

