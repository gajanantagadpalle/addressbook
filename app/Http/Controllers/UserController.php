<?php namespace App\Http\Controllers;

use Validator;
use Request;
use Auth;
use Hash;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show user profile and edit profile.
	 *
	 * @return Response
	 */
	public function Profile()
	{
		if(Request::isMethod('post') AND Request::ajax()){
			$retVal = array("status"=>0, "message"=>"Invalid Request");
			$name = trim(Request::input("name", ""));
			$validator = Validator::make(["name"=>$name], [
				'name' => 'required|max:255'
			]);

			if ($validator->fails()){
				$errors = $validator->errors();
    			$retVal["message"] = $errors->has('name') ? $errors->get('name') : "Invalid name";
    		}else{
    			$user = Auth::user();
    			$user->name = $name;
    			$user->save();
    			$retVal = array("status"=>1, "message"=>"Profile saved successfully");
    		}

    		return response()->json($retVal);
    	}else{
			return view('profile');
		}
	}

	/**
	 * Show Change Password page.
	 *
	 * @return Response
	 */
	public function ChangePassword()
	{
		if(Request::isMethod('post') AND Request::ajax()){
			$retVal = array("status"=>0, "message"=>"Invalid Request");
			$input = Request::only('password', 'password_confirmation');
			$validator = Validator::make($input, [
				'password' => 'required|confirmed|min:6',
			]);

			if ($validator->fails()){
				$errors = $validator->errors();
    			$retVal["message"] = $errors->has('password') ? $errors->get('password') : "Invalid password";
    		}else{
    			$password = Hash::make($input["password"]);
    			$user = Auth::user();
    			$user->password = $password;
    			$user->save();
    			$retVal = array("status"=>1, "message"=>"Password saved successfully");
    		}

    		return response()->json($retVal);
    	}else{
			return view('changepassword');
		}
	}

}
