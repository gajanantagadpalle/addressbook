<?php namespace App\Http\Controllers;
use App\Country;
use App\Addresstitle;
use App\Address;
use App\ToFrom;
use Validator;
use Auth;
use Request;
use DB;
class AddressController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Address Controller
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the Address list to the user.
	 *
	 * @return Response
	 */
	public function AddressList($user = false)
	{
		$user = Auth::user();
		$addresses = CommonController::getAddressList($user);
		return view('addresslist', compact('addresses'));
	}

	public function ActionDelAddress(){
		$user = Auth::user();
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		if(Request::isMethod('post')){
			$id = Request::input('id', 0);
			if($id){
				if(CommonController::deleteAddress($user, $id)){
					$retVal = array("status"=>1, "message"=>"Address deleted successfully");
				}
			}
		}
		return response()->json($retVal);
	}

	public function AddAddress($id = 0){


		$Addresstitles = Addresstitle::where("status", 1)->orderBy('title', 'asc')->get();
		$Countries = Country::where("status", 1)->orderBy('name', 'asc')->get();
		$ToFrom = Tofrom::where("status", 1)->orderBy('id', 'asc')->get();
		$Address =	Address::find($id);
		return view('addaddress', compact('Address', 'Addresstitles', 'Countries', 'ToFrom'));
	}

	public function ActionAddAddress(){
		$user = Auth::user();
		$retVal = CommonController::AddAddress($user);
		return response()->json($retVal);
	}

}
