<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Request;
use Validator;
class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function authenticate()
    {
    	$input = Request::only('username', 'password');

    	$validator = Validator::make($input, [
			'username' => 'required|alpha_num|max:255',
			'password' => 'required|min:6',
		]);

    	if ($validator->fails()){
    		return redirect('/auth/login')->withErrors($validator);
    	}

        if (\Auth::attempt($input))
        {
            return redirect()->intended('/');
        }
        $validator->getMessageBag()->add('password', 'Invalid username or password');
        return redirect('/auth/login')->withErrors($validator);
    }

}
