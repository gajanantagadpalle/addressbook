<?php namespace App\Http\Controllers;

use Request;
class ApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| API Controller
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('api');
	}

	public function getAddressList(Request $request){
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		$user = Request::has("x-user-Details") ? Request::input("x-user-Details"): false;
		if($user){
			$retVal["result"] = CommonController::getAddressList($user);
			$counts = count($retVal["result"]);
			$retVal["message"] = $counts . " records found";
			$retVal["status"] = ($counts) ? 1 : 0;
		}
		return response()->json($retVal);
	}

	public function deleteAddress(Request $request){
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		$user = Request::has("x-user-Details") ? Request::input("x-user-Details"): false;
		$id = Request::has("id") ? Request::input("id"): 0;
		if($user AND $id){
			if(CommonController::deleteAddress($user, $id)){
				$retVal = array("status"=>1, "message"=>"Address deleted successfully");
			}else{
				$retVal = array("status"=>0, "message"=>"Could not delete address, please provide correct details");
			}
		}
		return response()->json($retVal);
	}

	public function getAddressDetails(Request $request){
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		$user = Request::has("x-user-Details") ? Request::input("x-user-Details"): false;
		$id = Request::has("id") ? Request::input("id"): 0;
		if($user){
			$retVal["result"] = CommonController::getAddressList($user, $id);
			$counts = count($retVal["result"]);
			$retVal["message"] = $counts . " records found";
			$retVal["status"] = ($counts) ? 1 : 0;
		}
		return response()->json($retVal);
	}

	public function AddAddress(Request $request){
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		$user = Request::has("x-user-Details") ? Request::input("x-user-Details"): false;
		if($user){
			$retVal = CommonController::AddAddress($user);
		}
		return response()->json($retVal);
	}

}
