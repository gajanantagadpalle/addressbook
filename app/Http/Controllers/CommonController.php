<?php namespace App\Http\Controllers;
use App\Country;
use App\Addresstitle;
use App\Address;
use App\ToFrom;
use Validator;
use Auth;
use Request;
use DB;
class CommonController extends Controller {

	public static function getAddressList($user, $id=0)
	{
		$addresses = DB::table('addresses')
            ->join('addresstitles', 'addresstitles.id', '=', 'addresses.titleid')
            ->join('tofrom', 'tofrom.code', '=', 'addresses.tofromid')
            ->join('countries', 'countries.code', '=', 'addresses.country')
            ->select('addresses.id', 'addresses.person_name', 'addresses.person_phone', 'addresses.address_line_1', 'addresses.address_line_2', 'addresses.address_line_3', 'addresses.pincode', 'addresses.city', 'addresses.state', 'countries.name AS country', 'addresstitles.title', 'tofrom.name', 'addresses.created_at', 'addresses.updated_at')
            ->where("uid", $user->id);

            if($id){
				$addresses->where("addresses.id", $id);
				$addresses = $addresses->first();
            }else{
            	$addresses = $addresses->get();
            }


		return $addresses;
	}

	public static function getAddressDetails($id){

	}

	public static function deleteAddress($user, $id){
		$Address = Address::where("id",$id)->where("uid", $user->id)->first();
		if($Address){
			$Address->delete();
			return true;
		}
		return false;
	}

	public static function AddAddress($user){
		$retVal = array("status"=>0, "message"=>"Invalid Request");
		if(Request::isMethod('post')){
			$input = Request::only('tofromid', 'titleid', 'person_name', 'person_phone', 'address_line_1', 'address_line_2', 'address_line_3', 'pincode', 'city', 'state', 'country');
			$input["uid"] = $user->id;
			$id = Request::input('id', 0);
			$ValidatorRules = Address::getValidatorRules();
			$validator = Validator::make($input, $ValidatorRules);
			if($validator->fails()){
				$errors = $validator->errors();
				$retVal["message"] = implode('\n', $errors->all());
			}else{
				if($id){
					$address = Address::find($id);
					if($address){
						$address->tofromid = $input['tofromid'];
						$address->titleid = $input['titleid'];
						$address->person_name = $input['person_name'];
						$address->person_phone = $input['person_phone'];
						$address->address_line_1 = $input['address_line_1'];
						$address->address_line_2 = $input['address_line_2'];
						$address->address_line_3 = $input['address_line_3'];
						$address->pincode = $input['pincode'];
						$address->city = $input['city'];
						$address->state = $input['state'];
						$address->country = $input['country'];
						$address->uid = $user->id;
						$address->save();
						$retVal = array("status"=>1, "message"=>"Address updated successfully", "id"=>$address->id);
					}else{
						$retVal = array("status"=>0, "message"=>"Error in updating address");
					}

				}else{
					$address = Address::create($input);
					if($address){
						$retVal = array("status"=>1, "message"=>"Address inserted successfully", "id"=>$address->id);
					}else{
						$retVal = array("status"=>0, "message"=>"Error in inserting address");
					}

				}
			}
		}
		return $retVal;
	}

	public function docs(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		return view('docs', compact('api_token'));
	}


	public function testGetAddressList(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		$opts = [
		    "http" => [
		        "method" => "GET",
		        "header" => "x-api-token: " . $api_token
		    ]
		];
		$context = stream_context_create($opts);
		return file_get_contents('http://localhost:8080/api/getaddresslist', false, $context);
	}
	public function testDeleteAddress(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		$opts = [ "http" => [ "method" => "DELETE", "header" => "x-api-token:" . $api_token] ];
		$context = stream_context_create($opts);
		return file_get_contents('http://localhost:8080/api/deleteaddress?id=5', false, $context);
	}

	public function testGetAddressDetails(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		$opts = [ "http" => [ "method" => "GET", "header" => "x-api-token: " . $api_token] ]; $context = stream_context_create($opts); return file_get_contents('http://localhost:8080/api/getaddressdetails?id=3', false, $context);
	}

	public function testAddAddress(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		$postdata = http_build_query(
		    array(
		        'tofromid' => 'to_from',
		        'titleid' => '1',
		        'person_name' => 'Gajanan',
		        'person_phone' => '9096516143',
		        'address_line_1' => 'Vijay Garden',
		        'address_line_2' => 'GB Road',
		        'address_line_3' => 'Thane',
		        'pincode' => '400607',
		        'city' => 'Thane',
		        'state' => 'Maharashtra',
		        'country' => 'IN'
		    )
		);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => "x-api-token: ". $api_token
		    )
		);

		$context  = stream_context_create($opts);

		return file_get_contents('http://localhost:8080/api/addaddress?' . $postdata, false, $context);
	}


	public function testUpdateAddress(){
		$api_token = Auth::check()? Auth::user()->api_token : "<API_TOKEN>";
		$postdata = http_build_query(
		    array(
		        'id' => '7',
		        'tofromid' => 'to_from',
		        'titleid' => '1',
		        'person_name' => 'Gajanan',
		        'person_phone' => '9096516143',
		        'address_line_1' => 'Vijay Garden',
		        'address_line_2' => 'GB Road',
		        'address_line_3' => 'Thane',
		        'pincode' => '400607',
		        'city' => 'Thane',
		        'state' => 'Maharashtra',
		        'country' => 'IN'
		    )
		);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => "x-api-token: ". $api_token
		    )
		);

		$context  = stream_context_create($opts);

		return file_get_contents('http://localhost:8080/api/addaddress?' . $postdata, false, $context);
	}




}
