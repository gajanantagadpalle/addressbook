<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;
class Api {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$requestHeaders = apache_request_headers();
		$apitoken = (isset($requestHeaders['x-api-token']) AND trim($requestHeaders['x-api-token']) != "") ? trim($requestHeaders['x-api-token']) : "";
		if($apitoken != ""){
			$UserDetails = User::where("api_token", $apitoken)->first();
			if($UserDetails){
				$request->request->add(['x-user-Details' => $UserDetails]);
				return $next($request);
			}
		}
		 return response()->json(["status"=>0, "message"=>"Invalid Request"]);
	}

}
