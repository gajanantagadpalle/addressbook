<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model{
    protected $fillable = [
        'uid', 'tofromid', 'titleid', 'person_name', 'person_phone', 'address_line_1', 'address_line_2', 'address_line_3', 'pincode', 'city', 'state', 'country', 'status'
    ];

    public static function getValidatorRules(){
        return [
            'uid'=>'required|integer',
            'tofromid'=>'required|max:7',
            'titleid'=>'required|integer|max:5',
            'person_name'=>'required|max:255',
            'person_phone'=>'required|min:10|max:16',
            'address_line_1'=>'required',
            'address_line_2'=>'required',
            'address_line_3'=>'required',
            'pincode'=>'required|min:6|max:10',
            'city'=>'required|max:50',
            'state'=>'required|max:50',
            'country'=>'required|exists:countries,code'
        ];
    }
}
