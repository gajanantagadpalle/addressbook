@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.addaddress')}}</div>

				<div class="panel-body">
					<form id="ThisForm" class="form-horizontal" role="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" id="id" value="{{isset($Address->id) ? trim($Address->id) : 0}}">

						<div class="form-group">
							<label class="col-md-4 control-label">To Address And/Or From Address</label>
							<div class="col-md-8">
								<select name="tofromid" id="tofromid" >
									@foreach($ToFrom AS $thisToFrom)
										<option value="{{$thisToFrom->code}}"
											{{(isset($Address->tofromid) AND $Address->tofromid == $thisToFrom->code)? 'selected' : ''}}
											>{{$thisToFrom->name}}</option>
									@endforeach
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label">Title</label>
							<div class="col-md-8">
								<select name="titleid" id="titleid" >
									@foreach($Addresstitles AS $thisAddresstitle)
										<option value="{{$thisAddresstitle->id}}"
											{{(isset($Address->titleid) AND $Address->titleid == $thisAddresstitle->id)? 'selected' : ''}}
											>{{$thisAddresstitle->title}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Contact Person Name</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="person_name" value="{{isset($Address->person_name) ? trim($Address->person_name) : ''}}" id="person_name" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Contact Person Number</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="person_phone" value="{{isset($Address->person_phone) ? trim($Address->person_phone) : ''}}" id="person_phone" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="address_line_1" value="{{isset($Address->address_line_1) ? trim($Address->address_line_1) : ''}}" id="address_line_1" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="address_line_2" value="{{isset($Address->address_line_2) ? trim($Address->address_line_2) : ''}}" id="address_line_2" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address Line 3</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="address_line_3" value="{{isset($Address->address_line_3) ? trim($Address->address_line_3) : ''}}" id="address_line_3" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Pincode</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="pincode" value="{{isset($Address->pincode) ? trim($Address->pincode) : ''}}" id="pincode" required="required" maxlength="10">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">City</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="city" value="{{isset($Address->city) ? trim($Address->city) : ''}}" id="city" required="required" maxlength="50">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">State</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="state" value="{{isset($Address->city) ? trim($Address->city) : ''}}" id="state" required="required" maxlength="50">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-8">
								<select name="country" id="country" >
									@foreach($Countries AS $thisCountry)
										<option value="{{$thisCountry->code}}"
										{{(isset($Address->country) AND $Address->country == $thisCountry->code)? 'selected' : ''}}
											>{{$thisCountry->name}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Submit</button>
								<button type="reset" id="resetForm" class="hide"></button>
								<span id="errorMessage"></span>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$("#ThisForm").submit(function(e){
			e.preventDefault();
			$(".loading").show();
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "{{route('actionaddaddress')}}",
				data: $("#ThisForm").serialize(),
				success: function(result){
					$("#errorMessage").html(result.message);
					if(result.status){
						$("#id").val(result.id);
					}
					$(".loading").hide();
				},
				error: function(){
					$("#errorMessage").html("Error in submitting address.");
					$(".loading").hide();
				}
			});
		});
	});
</script>
@endsection
