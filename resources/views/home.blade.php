@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					Welcome in {{config('settings.app_name')}}!
					<br/>
					<ul>
						<li><a href="{{route('addaddress')}}">{{trans('site.addaddress')}}</a></li>
						<li><a href="{{route('addresslist')}}">{{trans('site.addresslist')}}</a></li>
						<li><a href="{{route('docs')}}">{{trans('site.docs')}}</a></li>
					<ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
