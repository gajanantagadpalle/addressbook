@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.profile')}}</div>

				<div class="panel-body">
					<form id="ThisForm" class="form-horizontal" role="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="name" value="{{Auth::user()->name}}" id="name" required="required" maxlength="255">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-8">
								<label class=" control-label">{{Auth::user()->username}}</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email ID</label>
							<div class="col-md-8">
								<label class=" control-label">{{Auth::user()->email}}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">API TOKEN</label>
							<div class="col-md-8">
								<label class=" control-label">{{Auth::user()->api_token}}</label>
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Submit</button>
								<span id="errorMessage"></span>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$("#ThisForm").submit(function(e){
			e.preventDefault();
			$(".loading").show();
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "{{route('profile')}}",
				data: $("#ThisForm").serialize(),
				success: function(result){
					$("#errorMessage").html(result.message);
					$(".loading").hide();
				},
				error: function(){
					$("#errorMessage").html("Error in updating profile.");
					$(".loading").hide();
				}
			});
		});
	});
</script>
@endsection
