@extends('app')

@section('content')
<style>
	code{
		word-wrap: break-word;
	}

</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.installationdocs')}}</div>
					<p>
						<h5>Server Requirements</h5>

						    The Laravel 5.0 framework has a few system requirements:<br/>
						    PHP >= 5.4, PHP < 7<br/>
						    Mcrypt PHP Extension<br/>
						    OpenSSL PHP Extension<br/>
						    Mbstring PHP Extension<br/>
						    Tokenizer PHP Extension<br/>
					</p>
					<br/><br/>
					<p>
						<h5>Create Mysql Database</h5>
						Create a database for this project. For example: addressbook
					</p>
					<br/><br/>
					<p>
						<h5>Download Project</h5>
							&nbsp;$ git clone https://gajanantagadpalle@bitbucket.org/gajanantagadpalle/addressbook.git<br/>
							&nbsp;$ cd addressbook<br/>
							&nbsp;$ composer update</br>
							&nbsp;$ create/modify .env file and add the database credentials</br>
							&nbsp;$ php artisan key:generate<br/>
							&nbsp;$ php artisan migrate:refresh --seed<br/>
							&nbsp;$ php artisan serve
					</p>

					<br/><br/>
					<p>
						<h5>Test Login</h5>
							&nbsp;Username:gajanantagadpalle<br/>
							&nbsp;Password:Test@1
					</p>
				<div class="panel-body">
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.apidocs')}}</div>

				<div class="panel-body">

					<p>
						<h5>To get the address list</h5>
						<h5>CURL</h5>
						<code>curl -X "GET" -H "x-api-token: {{$api_token}}" {{url('api/getaddresslist')}}
						</code>



							<h5>PHP</h5>
							<code>
$opts = [
"http" => [
    "method" => "GET",
    "header" => "x-api-token: {{$api_token}}"
]
];
$context = stream_context_create($opts);
return file_get_contents('http://localhost:8080/api/getaddresslist', false, $context);
					</code>
						<br/>
						<pre>Response:{"status":1,"message":"2 records found","result":[{"id":4,"person_name":"Gajanan Tagadpalle","person_phone":"9096516143","address_line_1":"mulund","address_line_2":"LBS road","address_line_3":"mumbai","pincode":"400607","city":"Mumbai","state":"Maharashtra","country":"India","title":"Home","name":"To Address","created_at":"2018-03-25 18:17:52","updated_at":"2018-03-25 18:17:52"},{"id":3,"person_name":"Gajanan Tagadpalle","person_phone":"8600135967","address_line_1":"vijay garden","address_line_2":"GB Road","address_line_3":"1-1-1124\/1, Near Pawde wadi naka, Near AK Sambhaji Mangal Karyalaya, Pushp Nagar Nanded 431602","pincode":"431602","city":"Nanded","state":"Nanded","country":"India","title":"Work","name":"From Address","created_at":"2018-03-25 16:04:24","updated_at":"2018-03-25 16:30:53"}]}</pre>

					</p>
					<br/><br/>
					<p>
						<h5>To add address</h5>
						<h5>CURL Code</h5>
						<code>curl -X "POST" -H "x-api-token: {{$api_token}}" {{url('api/addaddress')}} -d 'tofromid=to&titleid=1&person_name=Gajanan Tagadpalle&person_phone=9096516143&address_line_1=address1&address_line_2=address2&address_line_3=address3&pincode=400607&city=Thane&state=Maharashtra&country=IN'
						</code>
						<h5>PHP Code</h5>
						<code>
$postdata = http_build_query(
array(
'tofromid' => 'to_from',
'titleid' => '1',
'person_name' => 'Gajanan',
'person_phone' => '9096516143',
'address_line_1' => 'Vijay Garden',
'address_line_2' => 'GB Road',
'address_line_3' => 'Thane',
'pincode' => '400607',
'city' => 'Thane',
'state' => 'Maharashtra',
'country' => 'IN'
)
);

$opts = array('http' =>
array(
'method'  => 'POST',
'header'  => "x-api-token: ". $api_token
)
);

$context  = stream_context_create($opts);

return file_get_contents('http://localhost:8080/api/addaddress?' . $postdata, false, $context);
						</code>
						<table border="1" style="width:100%;">
							<tr><th>Field</th><th>Data type</th><th>Data</th></tr>
							<tr><td>tofromid (Mandatory)</td><td>String</td><td>to/from/to_from</td></tr>
							<tr><td>titleid (Mandatory)</td><td>Int</td><td>Reference id from 'addresstitles' table</td></tr>
							<tr><td>person_name (Mandatory)</td><td>String</td><td>Valid person name</td></tr>
							<tr><td>person_phone (Mandatory)</td><td>Int</td><td>Valid person phone</td></tr>
							<tr><td>address_line_1 (Mandatory)</td><td>String</td><td>Valid address_line_1</td></tr>
							<tr><td>address_line_2 (Mandatory)</td><td>String</td><td>Valid address_line_2</td></tr>
							<tr><td>address_line_3 (Mandatory)</td><td>String</td><td>Valid address_line_3</td></tr>
							<tr><td>pincode (Mandatory)</td><td>String</td><td>Valid pincode</td></tr>
							<tr><td>city (Mandatory)</td><td>String</td><td>Valid city</td></tr>
							<tr><td>state (Mandatory)</td><td>String</td><td>Valid state</td></tr>
							<tr><td>country (Mandatory)</td><td>String</td><td>Reference id from 'countries' table</td></tr>
						</table>
						<br/>
						<pre>Response:{"status":1,"message":"Address inserted successfully","id":4}</pre>


					</p>
<br/><br/>
					<p>
						<h5>To update address</h5>
						<h5>CURL Code</h5>
						<code>curl -X "POST" -H "x-api-token: {{$api_token}}" {{url('api/addaddress')}} -d 'id=1&tofromid=to&titleid=1&person_name=Gajanan Tagadpalle&person_phone=9096516143&address_line_1=address1&address_line_2=address2&address_line_3=address3&pincode=400607&city=Thane&state=Maharashtra&country=IN'</code>
						<h5>PHP Code</h5>
						<code>
$postdata = http_build_query(
array(
'id' => '1',
'tofromid' => 'to_from',
'titleid' => '1',
'person_name' => 'Gajanan',
'person_phone' => '9096516143',
'address_line_1' => 'Vijay Garden',
'address_line_2' => 'GB Road',
'address_line_3' => 'Thane',
'pincode' => '400607',
'city' => 'Thane',
'state' => 'Maharashtra',
'country' => 'IN'
)
);

$opts = array('http' =>
array(
'method'  => 'POST',
'header'  => "x-api-token: ". $api_token
)
);

$context  = stream_context_create($opts);

return file_get_contents('http://localhost:8080/api/addaddress?' . $postdata, false, $context);
						</code>
						<table border="1" style="width:100%;">
							<tr><th>Field</th><th>Data type</th><th>Data</th></tr>
							<tr><td>id (Mandatory)</td><td>Int</td><td>Valid id</td></tr>
							<tr><td>tofromid (Mandatory)</td><td>String</td><td>to/from/to_from</td></tr>
							<tr><td>titleid (Mandatory)</td><td>Int</td><td>Reference id from 'addresstitles' table</td></tr>
							<tr><td>person_name (Mandatory)</td><td>String</td><td>Valid person name</td></tr>
							<tr><td>person_phone (Mandatory)</td><td>Int</td><td>Valid person phone</td></tr>
							<tr><td>address_line_1 (Mandatory)</td><td>String</td><td>Valid address_line_1</td></tr>
							<tr><td>address_line_2 (Mandatory)</td><td>String</td><td>Valid address_line_2</td></tr>
							<tr><td>address_line_3 (Mandatory)</td><td>String</td><td>Valid address_line_3</td></tr>
							<tr><td>pincode (Mandatory)</td><td>String</td><td>Valid pincode</td></tr>
							<tr><td>city (Mandatory)</td><td>String</td><td>Valid city</td></tr>
							<tr><td>state (Mandatory)</td><td>String</td><td>Valid state</td></tr>
							<tr><td>country (Mandatory)</td><td>String</td><td>Reference id from 'countries' table</td></tr>
						</table>
						<br/>
						<pre>Response:{"status":1,"message":"Address updated successfully","id":4}</pre>
					</p>
<br/><br/>
					<p>
						<h5>To get the address details using id</h5>
						<h5>CURL Code</h5>
						<code>curl -X "GET" -H "x-api-token: {{$api_token}}" {{url('api/getaddressdetails?id=1')}}</code>

						<h5>PHP Code</h5>
							<code>
$opts = [ "http" => [ "method" => "GET", "header" => "x-api-token: " . $api_token] ]; $context = stream_context_create($opts); return file_get_contents('http://localhost:8080/api/getaddressdetails?id=3', false, $context);
					</code>


						<table border="1" style="width:100%;">
							<tr><th>Field</th><th>Data type</th><th>Data</th></tr>
							<tr><td>id (Mandatory)</td><td>Int</td><td>Valid id</td></tr>
						</table>
						<br/>
						<pre>Response:{"status":1,"message":"1 records found","result":{"id":4,"person_name":"Gajanan Tagadpalle","person_phone":"9096516143","address_line_1":"mulund","address_line_2":"LBS road","address_line_3":"mumbai","pincode":"400607","city":"Mumbai","state":"Maharashtra","country":"India","title":"Home","name":"To Address","created_at":"2018-03-25 18:17:52","updated_at":"2018-03-25 18:17:52"}}</pre>
					</p>
					<br/><br/>
					<p>
						<h5>To delete address</h5>
						<h5>CURL Code</h5>
						<code>curl -X "DELETE" -H "x-api-token: {{$api_token}}" {{url('api/deleteaddress')}} -d 'id=1'
						</code>
						<h5>PHP Code</h5>
							<code>
$opts = [ "http" => [ "method" => "DELETE", "header" => "x-api-token: {{$api_token}}"] ];
		$context = stream_context_create($opts);
		return file_get_contents('http://localhost:8080/api/deleteaddress?id=5', false, $context);
					</code>
						<table border="1" style="width:100%;">
							<tr><th>Field</th><th>Data type</th><th>Data</th></tr>
							<tr><td>id (Mandatory)</td><td>Int</td><td>Valid id</td></tr>
						</table>
						<br/>
						<pre>Response:{"status":1,"message":"Address deleted successfully"}</pre>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.apitesturl')}}</div>
					<p>


						To get address list : http://localhost:8000/testgetaddresslist<br/>
						To delete address :  http://localhost:8000/testdeleteaddress<br/>
						To get address details by id : http://localhost:8000/testgetaddressdetails<br/>
						To add address : http://localhost:8000/testaddaddress<br/>
						To update address : http://localhost:8000/testupdateaddress<br/>

						<br/><br/>
						Note:<br/>
						1) Please login to use these url<br/>
						2) While using test url, PHP Code may not work on same port. So try running two different sites on different ports and check.

					</p>
				<div class="panel-body">
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){

	});
</script>
@endsection
