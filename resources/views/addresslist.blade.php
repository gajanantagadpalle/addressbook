@extends('app')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.jqueryui.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<style type="text/css">
	.actionimages{
		width: 15px;
		height: 15px;
		cursor: pointer;
	}
	.deleteaddress img{
		height: 14px;
		margin-left: 5px;
	}
	.sorting,.sorting_asc,.sorting_desc{
		cursor: pointer;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('site.addresslist')}}</div>
				<div class="panel-body">
					<table id="AddressTable" class="display" style="width:100%">
				        <thead>
				            <tr>
				                <th>Person Name</th>
				                <th>Person Phone</th>
				                <th>FROM / TO Address</th>
				                <th>Complete Address</th>
				                <th>Pincode</th>
				                <th>City</th>
				                <th>State</th>
				                <th>Country</th>
				                <th>Modified Time</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($addresses AS $thisAddress)
				            <tr>
				                <td>{{$thisAddress->person_name}}</td>
				                <td>{{$thisAddress->person_phone}}</td>
				                <td>{{$thisAddress->name}}</td>
				                <td>{{$thisAddress->address_line_1 . ' ' . $thisAddress->address_line_2 . ' ' . $thisAddress->address_line_3}}</td>
				                <td>{{$thisAddress->pincode}}</td>
				                <td>{{$thisAddress->city}}</td>
				                <td>{{$thisAddress->state}}</td>
				                <td>{{$thisAddress->country}}</td>
				                <td>{{$thisAddress->updated_at}}</td>
				                <td><a class="actionlinks editaddress" href="{{route('addaddress', [$thisAddress->id])}}"><img class="actionimages" src="{{url('assets/img/edit.svg')}}"></a><a class="actionlinks deleteaddress" data-id="{{$thisAddress->id}}"><img class="actionimages" src="{{url('assets/img/trash-alt.svg')}}"></a></td>
				            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$('#AddressTable').DataTable();
		$(".deleteaddress").click(function(){
			deleteid = $(this).attr("data-id");
			if(deleteid > 0){
				if(confirm("Are you sure want to delete this address?")){
					$.ajax({
					type: "POST",
					dataType: "JSON",
					url: "{{route('actiondeladdress')}}",
					data: {"_token":"{{ csrf_token() }}", "id":deleteid},
					success: function(result){
						document.location.reload();
					},
					error: function(){
						$(".loading").hide();
						alert("Error in submitting address.");
					}
				});
				}
			}
		});

		$("#ThisForm").submit(function(e){
			e.preventDefault();
			$(".loading").show();
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "{{route('actionaddaddress')}}",
				data: $("#ThisForm").serialize(),
				success: function(result){
					$("#errorMessage").html(result.message);
					if(result.status){
						$("#id").val(result.id);
					}
					$(".loading").hide();
				},
				error: function(){
					$("#errorMessage").html("Error in submitting address.");
					$(".loading").hide();
				}
			});
		});
	});
</script>
@endsection
