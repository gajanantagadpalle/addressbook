<?php

return [

	'addresslist'=>"Address List",
	'addaddress'=>"Add Address",
	'profile'=>"Profile",
	'changepassword'=>"Change Password",
	'docs'=>"Documentation",
	'installationdocs'=>"Installation Documentation",
	'apidocs'=>"API Documentation",
	'apitesturl'=>"API Test URL",

];
